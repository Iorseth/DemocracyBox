## Install Dependencies

```
# apt-get install php5 php5-sqlite php5-gd unzip
```

## Download and install Kanboard

```
$ wget https://kanboard.net/kanboard-latest.zip
$ unzip kanboard-latest.zip
# chown -R www-data:www-data /home/pi/kanboard/data
$ rm kanboard-latest.zip
```
## Create Vhost for your installation

```
# vi /etc/apache/sites-available/kanboard.conf
```
```
<VirtualHost *:80>
        ServerName kanboard.dmbox.lan

        DocumentRoot /path/kanboard

         <Directory /path/kanboard>
                Options FollowSymlinks
                Require all granted
                RewriteEngine On
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

```

### Active this vhost and reload the webserver

```
# a2ensite kanboard.conf
# service apache2 restart
```

## Last step

- You can log with default login "admin" and password "admin"
