## Install Dependencies

```
# apt-get install apache2 php5 php5-fpm mysql-server php5-mysql libapache2-mod-php5
$ wget http://wordpress.org/latest.tar.gz
$ tar -xzvf latest.tar.gz
```

### If you prefer a french installation, download from this link
```
$ wget https://fr.wordpress.org/wordpress-4.6.1-fr_FR.zip
$ unzip wordpress-4.6.1-fr_FR.zip
```
## Create database

```
# mysql
mysql> create database wordpress;
mysql> grant all privileges on wordpress.* to wordpress@localhost identified by '[motdepasse]';
mysql> flush privileges; 
mysql> exit

```

## Create Vhost for your installation

```
# vi /etc/apache2/sites-available/wordpress.conf
```
```
<VirtualHost *:80>
        ServerName www.dmbox.lan

        DocumentRoot /path/wordpress        #ex : /home/pi/wordpress

         <Directory /path/wordpress>            
                Options FollowSymlinks      
                Require all granted
                RewriteEngine On
       #        Include /path/wordpress/.htaccess
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

```

### Active this vhost and reload the webserver

```
# a2enmod rewrite
# a2ensite wordpress.conf
# service apache2 restart
```