## Install Dependencies
### Be root for all of the following commands
```
# apt-get install dnsmasq hostapd

```

## Configure the network of your access point in /etc/network/interfaces

Wlan 1 is by default the interface of your access point, so edit this configration files for give this static address
Delete all existing parameters for wlan1

```
allow-hotplug wlan1
iface wlan1 inet static # Interface Wlan1 ts defined with static address
    address 192.168.2.1 #The Ip of your local network
    netmask 255.255.255.0 #The masq of your local network
    network 192.168.2.0 # The Ip of the network
    broadcast 192.168.2.255 # The broadcast
```

```
# ifdown wlan1
# ifup wlan1
# systemctl restart networking
```
## create and add the following configuration files to /etc/hostapd/hostapd.conf

```
 nano /etc/hostapd/hostapd.conf
```

```
interface=wlan1
driver=nl80211
ssid=DemocracyBox
hw_mode=g
channel=6
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=piratebox
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP




```
- ctrl +x for save your files

### And add the following line to /etc/default/hostapd

```
DAEMON_CONF="/etc/hostapd/hostapd.conf"

```
## Add the following configuration files to /etc/dnsmasq.conf

```
# nano /etc/dnsmasq.conf
```
It's here that we edit the range of ip adress for clients, and we edit local domain for Apache vhost

```
#resolv-file=/tmp/resolv.conf.auto
domain=dmbox.lan
interface=wlan1      # Use interface wlan0
listen-address=192.168.2.1 # Explicitly specify the address to listen on
bind-interfaces      # Bind to the interface to make sure we aren't sending things elsewhere
dhcp-range=192.168.2.10,192.168.2.240,12h
address=/kanboard.dmbox.lan/192.168.2.1
address=/www.dmbox.lan/192.168.2.1
address=/limesurvey.dmbox.lan/192.168.2.1
address=/pad.dmbox.lan/192.168.2.1
address=/chat.dmbox.lan/192.168.2.1
address=/democracyos.dmbox.lan/192.168.2.1

```
- ctrl +x for save your files

## Finish by restart services

```
reboot

```

If it doesn't works, reboot your system.