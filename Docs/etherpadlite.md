## Install Dependencies

```
# apt-get install gzip git curl python libssl-dev pkg-config build-essential npm libapache2-mod-proxy-html libxml2-dev apache2
```

## Clone the source on the server ( with current user)

```
git clone git://github.com/ether/etherpad-lite.git
```

- Now, you could the script, and it wil work on your rasp

## Create the vhost for etherpadlite

```
<VirtualHost *:80>
      
  ServerName pad.dmbox.lan
  ProxyRequests On
  ProxyPreserveHost On
  ErrorLog /var/log/pad.dmbox.lan_error.log
  TransferLog /var/log/pad.dmbox.lan_access.log


         <Location />
                Require all granted
                ProxyPass http://127.0.0.1:9001/
                ProxyPassReverse http://127.0.0.1:9001/
        </Location>

</VirtualHost>


```

-and active modules

```
# a2enmod proxy
# a2enmod proxy_http
# a2ensite pad.conf
# service apache2 restart

```
## Listen on localhost
- Open etherpad-lite/settings.json and change
```
//IP and port which etherpad should bind at
  "ip": "0.0.0.0",
```
- bi
```
//IP and port which etherpad should bind at
  "ip": "127.0.0.1",
```
## Create an automate script for run etherpadlite

- Create file in /etc/systemd/system/pad.service

```
[Unit]
Description=etherpadlite

[Service]
ExecStart=/bin/bash /home/pi/etherpad-lite/bin/run.sh
Restart=always
RestartSec=10
User=pi
Group=pi

[Install]
WantedBy=multi-user.target




```

- and active it 

```
# systemctl daemon-reload
# systemctl start pad.service
# systemctl enable pad.service
```