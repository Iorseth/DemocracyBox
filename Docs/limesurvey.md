## Install Dependencies

```
# apt-get install apache2 php5 php5-fpm mysql-server php5-mysql
$ wget https://www.limesurvey.org/stable-release?download=1934:limesurvey257%20161202zip
$ mv limesurvey257%20161202zip limesurvey257%20161202.zip
$ unzip limesurvey257%20161202.zip
```

## Create database

```
# mysql
mysql> create database limesurvey;
mysql> grant all privileges on limesurvey.* to limesurvey@localhost identified by 'motdepasse';
mysql> flush privileges; 
mysql> exit

```

## Create Vhost for your installation

```
# vi /etc/apache2/sites-available/limesurvey.conf
```
```
<VirtualHost *:80>
        ServerName limesurvey.dmbox.lan

        DocumentRoot /path/limesurvey # ex: /home/pi/limesurvey

         <Directory /path/limesurvey>
                Options FollowSymlinks
                Require all granted
                RewriteEngine On
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

```

### Active this vhost and reload the webserver

```
# a2enmod rewrite
# a2ensite limesurvey.conf
# service apache2 restart
```